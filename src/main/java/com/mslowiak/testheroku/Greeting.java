package com.mslowiak.testheroku;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class Greeting {
    private String msg;

    public String createGreeting(String type) {
        if (type.equals("nice")) {
            return "Hi, " + msg;
        } else {
            return "Helldsadasdo, " + msg;
        }
    }
}
