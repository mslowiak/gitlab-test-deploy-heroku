package com.mslowiak.testheroku;

import org.reactivestreams.Publisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class GreetReactiveController {
    @GetMapping
    public Publisher<Greeting> greetingPublisher() {
        return Flux.<Greeting>generate(sink -> sink.next(new Greeting("aaaaHello"))).take(50);
    }
}
