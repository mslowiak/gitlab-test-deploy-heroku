package com.mslowiak.testheroku;

import org.junit.Assert;
import org.junit.Test;

public class GreetingTest {
    @Test
    public void testNiceMessage() {
        Greeting greeting = new Greeting("its sunny");

        String nice = greeting.createGreeting("nice");

        Assert.assertEquals("Hi, its sunny", nice);
    }
}